//
//  Activity.swift
//  iOsProject
//
//  Created by user187640 on 4/7/21.
//

import Foundation



struct Activity: Codable{
    var id: String
    var fields: ActivityFields

}

struct ActivityResults: Codable{
    let records: [Activity]?
}


struct ActivityFields: Codable{

   
    var name: String
    var type: String
    var start: String
    var end: String
    var location: [String]
    var notes : String
    
    

    
    enum CodingKeys: String, CodingKey {
            case location = "Location"
            case name = "Activity"
            case type = "Type"
            case start = "Start"
            case end = "End"
            case notes = "Notes"
            
        }
    
}
