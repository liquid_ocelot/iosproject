import Foundation

struct Sponsor : Codable {
    var fields: SponsorFields
    
}

struct SponsorRecords: Codable {
    let records: [Sponsor]?
}

struct SponsorFields: Codable {
    var companyName: String
    var status: String
    var sponsoredAmount: Int?
    var notes: String? //may be empty in the API
    //var contacts: [Contact]? //may be empty in the API

    
    enum CodingKeys: String, CodingKey {
        case companyName = "Company"
        case status = "Status"
        case sponsoredAmount = "Sponsored amount"
        case notes = "Notes"
    }
}
