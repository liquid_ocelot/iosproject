import Foundation

struct Speakers : Codable {
    var fields: SpeakersFields
    
}

struct SpeakerRecords: Codable {
    let records: [Speakers]?
}

struct SpeakersFields: Codable {
    var company: [String?]?
    var email: String?
    var name: String?
    var phone: String?
    var role: String?
    var type: String?
    
    enum CodingKeys: String, CodingKey {
        case company = "Company"
        case email = "Email"
        case name = "Name"
        case phone = "Phone"
        case role = "Role"
        case type = "Type"
    }
}
