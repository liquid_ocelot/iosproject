//
//  ActivityViewController.swift
//  iOsProject
//
//  Created by user187640 on 3/31/21.
//

import Foundation
import UIKit

class ActivityViewController: UIViewController{
    @IBOutlet weak var noteField: UITextView!
    @IBOutlet weak var activityTitle: UINavigationBar!
    
    @IBOutlet weak var endDate: UILabel!
    @IBOutlet weak var startDate: UILabel!
    var activity: Activity?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadActivity(id:"rec38jO5T05cqdJDx")
        
        
        
    }
    
    
    func loadActivity(id:String){
        let requestFactory = RequestFactory()
        
        requestFactory.getActivity(id:id){
            
                        (errorHandle, activityAPI) in
                        if let _ = errorHandle.errorType,
                        let errorMessage = errorHandle.errorMessage {
                            print(errorMessage)
                        }
                        else if activityAPI != nil {
                            //print(sponsorsList?.last?.fields.companyName)
                            self.activity = activityAPI!
                            print("kalm")
                            
                            
                            DispatchQueue.main.async {
                                self.noteField.text = self.activity?.fields.notes
                                self.activityTitle.topItem?.title = self.activity?.fields.name
                                self.startDate.text = self.activity?.fields.start
                                self.endDate.text = self.activity?.fields.end
                            }
                            
                        }
                        else {
                            print("panik")
                        }

        }
    }
    
    
    
}
