import Foundation
import UIKit

class SponsorsListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    var sponsors: [Sponsor] = [Sponsor]()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        self.title = "Sponsors List"
       
        self.loadSponsors()
        tableView.reloadData()

    }
    
    func loadSponsors() {
        let requestFactory = RequestFactory()

        requestFactory.getSponsorList {
            (errorHandle, sponsorsList) in
            if let _ = errorHandle.errorType,
            let errorMessage = errorHandle.errorMessage {
                print(errorMessage)
            }
            else if sponsorsList != nil {
                //print(sponsorsList?.last?.fields.companyName)
                self.sponsors = sponsorsList!
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }

            }
            else {
                print("Houston we got a problem Here!!")
                self.sponsors = [Sponsor]()
            }
            
        }
    
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sponsors.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SponsorCell = tableView.dequeueReusableCell(withIdentifier: "sponsorCell", for: indexPath) as! SponsorCell
        let sponsor = sponsors[indexPath.row]
        
        cell.setUpCell(sponsor: sponsor)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let senderCell = sender as? SponsorCell,
           let controller: SponsorsDetailViewController = segue.destination as? SponsorsDetailViewController {
            controller.sponsor = senderCell.sponsor
            
        }
    }


}
