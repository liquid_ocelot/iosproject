import Foundation
import UIKit

class SponsorsDetailViewController: UIViewController {
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var sumSponsoredLabel: UILabel!
    @IBOutlet weak var notesTextView: UITextView!
    var sponsor: Sponsor?
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpPage()
    }

    
    private func setUpPage() {
        if let sponsor = self.sponsor {
            self.companyNameLabel.text = sponsor.fields.companyName
            self.sumSponsoredLabel.text = String(sponsor.fields.sponsoredAmount!)
            self.notesTextView.text = sponsor.fields.notes!
        }
    }
}
