
import Foundation
import UIKit

class SponsorCell: UITableViewCell {

    @IBOutlet weak var companyName: UILabel!
    var sponsor: Sponsor?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setUpCell(sponsor: Sponsor) {
        self.sponsor = sponsor
        self.companyName.text = sponsor.fields.companyName
    }
}
