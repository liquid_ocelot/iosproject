//
//  detailsPeopleController.swift
//  iOsProject
//
//  Created by Alexandre Gomez on 31/03/2021.
//

import Foundation
import UIKit

class detailsPeopleController: UIViewController {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelType: UILabel!
    @IBOutlet weak var labelCompany: UILabel!
    @IBOutlet weak var labelRole: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    
    @IBOutlet weak var labelPhone: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        fetchedDataFromApi()
        loadSpeakers()
    }


    func fetchedDataFromApi(){
        //API Call
        //Data
        labelName.text = "Default"
        labelType.text = "Default";
        labelCompany.text = "Default";
        labelRole.text = "Default";
        labelEmail.text = "Default";
        labelPhone.text = "Default";
    }
    
    func loadSpeakers() {
           let requestFactory = RequestFactory()

           requestFactory.getSpeakerList {
               (errorHandle, speakersList) in
               if let _ = errorHandle.errorType,
               let errorMessage = errorHandle.errorMessage {
                   print(errorMessage)
               }
               else if speakersList != nil {
                    let speaker = speakersList?.first
                    DispatchQueue.main.async {
                        self.labelName.text = speaker?.fields.name
                        self.labelType.text = speaker?.fields.type
                        //self.labelCompany.text = "Default";
                        self.labelRole.text = speaker?.fields.role
                        self.labelEmail.text = speaker?.fields.email
                        self.labelPhone.text = speaker?.fields.phone
                    }
               }
               else {
                   print("Houston we got a problem Here!!")
               }
               
           }
       }

}
